//
// Copyright 2013,  Stefan Lundgren,   onehourgroup.com  
//

function laRenderImage( object, context, preScaled )
{
    if( 'image' in object )
    {
		var image = object.image;
		
	    if( preScaled ) //'scaleCanvas' in object && object.scaleCanvas.width > 0 && object.scaleCanvas.height > 0  )
		{
			_laDrawPreScaled( object, context );			
		}
		else	
			context.drawImage( image, object.alignment.x, object.alignment.y );
	}
}


function laRenderRange( object, context, preScaled )
{
    // Draw cannon range circle
    if( 'range' in object && 'selected' in object )
    {
		if( object.selected == true )
		{
			context.save();
			
			if( preScaled )
   				context.scale( laGlobalScale, laGlobalScale );
   				
			context.fillStyle	= "#fff";
			context.beginPath();
			var saveGA			= context.globalAlpha;
			context.globalAlpha = Math.min( saveGA, 0.1 );
			context.arc(0, 0,object.range,0,Math.PI*2, true);
			context.fill();
			context.globalAlpha = saveGA;
			context.restore();
		}
    }
}

function laRenderTalkArrow( arrow, context )
{
	var object = arrow.dialog;
	
	if( object.removed )
	{
		laRemoveObject( arrow );
		return;
	}
	
	if( !object.visible )
		return;
	
	context.fillStyle	= "#000";
	var saveGA			= context.globalAlpha;
	context.globalAlpha = Math.min( saveGA, 0.5 );
		
	
	if( 'talkTarget' in object )
	{
		var above = false;
		var left = false;
		
		if( object.talkTarget.x < object.position.x)
			left = true;
		
		if( object.talkTarget.y < object.position.y )
			above = true;		
		
		context.lineWidth = 12;

		if( left )
		{
			context.beginPath();
			context.moveTo( Math.round(object.boundary.width*0.1  - object.boundary.width/2) ,Math.round(-object.boundary.height/2));
			context.lineTo( Math.round(object.boundary.width*0.15 - object.boundary.width/2) ,Math.round(-object.boundary.height*0.2-object.boundary.height/2));
			context.lineTo( Math.round(object.boundary.width*0.2  - object.boundary.width/2) ,Math.round(-object.boundary.height/2));
			context.fill();
		}
	}	
	

}

function laRenderDialog( object, context )
{	
	context.fillStyle	= "#000";
	var saveGA			= context.globalAlpha;
	context.globalAlpha = Math.min( saveGA, 0.5 );
	context.fillRect (-Math.round(object.boundary.width/2), -Math.round(object.boundary.height/2), Math.round(object.boundary.width), Math.round(object.boundary.height));	
	
	context.globalAlpha = saveGA;
	
	
	if( 'text' in object.dialog )
	{	
		var letterWidth  = object.dialog.font.letterWidth;
		var letterHeight = object.dialog.font.letterHeight;
		
		var lettersPerRow = Math.floor((object.boundary.width  - letterWidth  * 2 ) / letterWidth);
		var rows          = Math.floor((object.boundary.height - letterHeight * 2 ) / letterHeight);
		
		if ( lettersPerRow <= 0 || rows <= 0 )
			return;
		
		var xStart = -Math.round(object.boundary.width/2) + letterWidth;
		var row = 0; 
		var textNext = ""; 
		var textBreak = ""; 
		var useBreakText = false;
		
		for( var charIndex = 0; charIndex <= object.dialog.text.length; charIndex++ )
		{
			var theChar = " ";
			
			if( charIndex < object.dialog.text.length )
				theChar = object.dialog.text.charAt( charIndex );
			
			var lastCharSpace = false;	
			if( theChar == ' ' )
			{
				if( useBreakText )
				{
					textNext += textBreak;
					textBreak = "";
				}
				
				useBreakText  = true;
				lastCharSpace = true;	
			}
			
			if( textNext.length + textBreak.length == lettersPerRow ||  charIndex >= object.dialog.text.length )
			{
				var y = -Math.round(object.boundary.height/2) + letterHeight + letterHeight * row;
				laDrawFontTextXY( context, object.dialog.font, textNext, xStart , y, 'top-left');
				textNext = textBreak;
				textBreak = "";
				row++;
				
				if( row >= rows )
					break;
					
				useBreakText = false;
				
				if( lastCharSpace )
					continue;
			}
			
			if( useBreakText && !lastCharSpace)
				textBreak += theChar;
			else
				textNext += theChar;
		}
	}	
}

function laRenderText( object, context )
{
	// Draw text
	if( 'text' in object )
	{	
		laDrawFontTextXY( context, object.font, object.text, object.alignment.x , object.alignment.y);
	}
}

function laRenderHealth( object, context, preScaled )
{
    // Draw health line.
    if( object.maxHealth > 0 )
    {
		context.save();
		
		if( preScaled )
   			context.scale( laGlobalScale, laGlobalScale );
	
		if( object.health/object.maxHealth > 0.67 )
			context.fillStyle = "#0f0";
		else if( object.health/object.maxHealth > 0.34 )
			context.fillStyle = "#FF0";
		else
			context.fillStyle = "#F00";
			
        context.fillRect (-Math.round(object.boundary.width/2) ,- Math.round(object.boundary.height/2), Math.round(object.boundary.width*object.health/object.maxHealth),Math.round(3));
        context.restore();
    }
}

function laRenderFireStatus( object, context )
{
    // Draw fire status.
    if( object.fireDelay > 0 )
    {
        context.fillStyle = "#00c";
        context.fillRect (-Math.round(object.boundary.width/2)-2, -2 - Math.round(object.boundary.height/2),2,Math.round(object.boundary.height*object.fireCounter/object.fireDelay));
    }
}


function laRenderSelectedGrid( object, context, preScaled, strokeStyle )
{
	if ( strokeStyle == null )
		strokeStyle = "#0c0"
	
    // Draw selection.
    if( object.selected )
    {
    	context.save();
    	
    	if( preScaled )
    		context.scale( laGlobalScale, laGlobalScale );
    		
		context.lineWidth = 3;
        context.beginPath();
        context.strokeStyle = strokeStyle;
        
        var x = object.position.x;
        var y = object.position.y;
                
        posX = -Math.round( (x) % (laGridSize ) ) ;
        posY = -Math.round( (y) % (laGridSize ) ) ;

        context.moveTo( posX , posY );
        context.lineTo( posX + object.boundary.width, posY );
        context.lineTo( posX + object.boundary.width, posY + object.boundary.height );
        context.lineTo( posX, posY + object.boundary.height );
        context.closePath();
        context.stroke();
        context.restore();
    }
}


function laRenderSelected( object, context, preScaled, strokeStyle )
{
	if ( strokeStyle == null )
		strokeStyle = "#0c0"
	
    // Draw selection.
    if( object.selected )
    {
    	context.save();
    	
    	if( preScaled )
    		context.scale( laGlobalScale, laGlobalScale );
    		
		context.lineWidth = 3;
        context.beginPath();
        context.strokeStyle = strokeStyle;
        
        var x = object.position.x;
        var y = object.position.y;

                
        context.moveTo( object.alignment.x, object.alignment.y );
        context.lineTo( object.alignment.x + Math.round(object.boundary.width), object.alignment.y  );
        context.lineTo( object.alignment.x + Math.round(object.boundary.width), object.alignment.y + Math.round(object.boundary.height) );
        context.lineTo( object.alignment.x,  object.alignment.y + Math.round(object.boundary.height) );
        context.closePath();
        context.stroke();
        context.restore();
    }
}


// Display the targets in order with a line
function laRenderTargetsLine( object, context )
{
	context.save()

    if( 'targets' in object && object.targets.length > 0)
    {
		context.globalAlpha = 0.2
		context.lineWidth = 6;
		context.moveTo(object.targets[0].position.x , object.targets[0].position.y );
		context.beginPath();
		context.strokeStyle = "#ccc";

		for(var	j =	0; j < object.targets.length; j ++)
			context.lineTo(object.targets[j].position.x , object.targets[j].position.y);

		context.stroke();
		context.lineWidth = 1;
		context.globalAlpha = 1;
	}
	context.restore();
}

function laRenderAddShadow( object, context ) 
{
	context.shadowOffsetX	= Math.round(2 * laGlobalScale );
	context.shadowOffsetY	= Math.round(8 * laGlobalScale );
	context.shadowBlur		= 0;
	context.shadowColor		= 'rgba(0, 0, 0, 0.2)';
} 

function laRenderRemoveShadow( object, context )
{
	context.shadowOffsetX	= 0;
	context.shadowOffsetY	= 0;
	context.shadowBlur		= 0;
	context.shadowColor		= 'rgba(0, 0, 0, 0)';
}


// Engine internal functions

// Just draw prescaled image from off screen canvas
function _laDrawPreScaled( object, context )
{	
	var sc = object.scaleCanvas;

	if( sc.width == 0 || sc.height == 0  )
		return;
	
	if( object.alignment.type == "top-left" )
		context.drawImage( sc, 0, 0 );
	else
		context.drawImage( sc, -Math.round( sc.width/2 ), -Math.round( sc.height/2  ));


	if( laDbgMode == laDebugModeEx && object.alignment.type != "top-left")
	{
		context.save()
		context.globalAlpha = 0.2;
		context.fillStyle = "#c00";
		context.fillRect (	Math.round( ( -sc.width /2 )), 
							Math.round( ( -sc.height/2 )),
							sc.width, 
							sc.height );
		context.restore();
	}			
}
