//
// Copyright 2013,  Stefan Lundgren,  onehourgroup.com  
//

include('./../la-engine/la_database.js');
include('./../la-engine/la_object_engine.js');
include('./../la-engine/la_object_children.js');
include('./../la-engine/la_object_properties.js');
include('./../la-engine/la_object_callbacks.js');

include('./../la-engine/la_update_engine.js');
include('./../la-engine/la_update_function.js');
include('./../la-engine/la_update_utilities.js');

include('./../la-engine/la_render_engine.js');
include('./../la-engine/la_render_function.js');

include('./../la-engine/la_event_engine.js');


include('./../la-engine/la_utilities.js');
include('./../la-engine/la_font.js');

include('./../la-engine/la_input.js');
include('./../la-engine/la_audio.js');
include('./../la-engine/la_debug.js');

include('./../la-engine/la_database.js');




function include(filename)
{
	var head	= document.getElementsByTagName('head')[0];
	
	var script		= document.createElement('script');
	script.src	= filename;
	script.type = 'text/javascript';
	
	head.appendChild(script);
}

