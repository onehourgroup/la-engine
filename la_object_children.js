//
// Copyright 2013,  Stefan Lundgren,  onehourgroup.com  
//


// Creates an empty object in the current world.
function laCreateChildObject( parent )
{
	return laCreateChildObjectClone( parent, new laObject() );
}


// Creates an object copy from any object and inserts into current world.
function laCreateChildObjectClone( parent, object )
{
	if( !('childObjects' in parent ) )
	{
		parent.childCount   = 0;
		parent.childObjects = [];

		laAddRenderFunction( parent, 'children', laRenderChildren );			
	}
	
	parent.childCount++;
	
	var objectClone = new laObject();
	laCopy( object, objectClone );
	objectClone.child = true;
	_laAddToUpdateList( objectClone );
	
	_laVisitChildren( objectClone, _laAddToUpdateList );
	return (parent.childObjects[parent.childCount-1] = objectClone);
}

function laSetChildId( child, id )
{
	child.childId = id;
}

function laGetChild( object, id )
{
	// Linear find first child. TODO optimize.
	if( 'childObjects' in object )
	{
		objects = object.childObjects;
		for( var i=0; i < objects.length; i++ )
		{
			child = objects[i];
			if( 'childId' in child && child.childId == id )
				return child;
		}
	}
	return null;
}

function laRemoveChild( object, id )
{
	// Linear find first child. TODO optimize.
	if( 'childObjects' in object )
	{
		objects = object.childObjects;
		for( var i=0; i < objects.length; i++ )
		{
			child = objects[i];
			if( 'childId' in child && child.childId == id )
			{
				parent.childCount--;
				return objects.splice(i,1);
			}
		}
	}
	return null;
}


function laRenderChildren( object, context )
{
	// Render children
	if( 'childObjects' in object )
	{
		var childObjects = object.childObjects;
		
		for( var i=0; i < childObjects.length; i=i+1 )
		{
			var child = childObjects[i];
			if( child.visible )
				laDrawObject( childObjects[i], context, true );	
		}
	}
}

//
//  Internal functions used by engine:
//


function _laVisitChildren( object, operator )
{
	if( 'childObjects' in object )
	{
		var objects = object.childObjects;
		for( var i=0; i < objects.length; i++ )
		{
			operator( objects[i] );
			_laVisitChildren( objects[i], operator );
		}
	}
}