//
// Copyright 2013,  Stefan Lundgren,  onehourgroup.com  
//
var laTargetDeadCallback			= false;
var laTargetHitCallback				= false;
var laTargetMissedCallback			= false;

// Callback function should have two parameters. ( KilledObject, KillerObject )
function laSetTargetDeadCallback( callback )
{
	laTargetDeadCallback = callback;
}

// Callback function should have two parameters. ( KilledObject, KillerObject )
function laSetTargetHitCallback( callback )
{
	laTargetHitCallback = callback;
}

// Callback function should have two parameters. ( KilledObject, KillerObject )
function laSetTargetMissedCallback( callback )
{
	laTargetMissedCallback = callback;
}

function laTargetActionDamage( object, target )
{
	// Enemy and bullets hit detection
	if( 'damage' in object && 'health' in target )
	{		
		if( target.health > 0 )
		{
			target.health -= object.damage;

			if( target.health <= 0 )
			{
				if( laTargetDeadCallback != false )
				{
					laTargetDeadCallback( target, object );
				}
			}
			else if( laTargetHitCallback != false )
			{
				laTargetHitCallback( target, object );
			}			
		}
		else if( laTargetMissedCallback != false )
			laTargetMissedCallback( target, object );
	}
	
	laRemoveObject( object );
}



// Set an action function callback to be called when an object reaches its target.
function laSetTargetAction( object, targetCb )
{
	object.targetAction     = targetCb;
}


// Set an action function callback to be called when an object reaches a checkpoint.
function laSetCheckpointAction( object, checkPointCb )
{
	object.checkPointAction = checkPointCb;
}

function laSetSelectedCallback( object, selectedCb )
{
	object.selectedCallback    = selectedCb;	
}

function laRemoveSelectedCallback( object )
{
	delete object.selectedCallback;
}

