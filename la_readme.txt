
In your HTML file include the following to include all laEngine code:

<script type="text/javascript" src="./../la-engine/la_lib.js"></script>


Start your script by setting the window.onload property to a function that calls the laInitEngine with a parameter that is a function to your code.

window.onload =	function () {laInitEngine(myScriptThatUseLaEngine);};



Showing an image:

function myScriptThatUseLaEngine()
{
   var background = laCreateObject();
   laSetBackgroundFromPath( background, "image.png", "Fit" );
   laSetObjectAlignment( background, "top-left" );

   laFadeIn();
   laStartEngine();
}