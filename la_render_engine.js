//
// Copyright 2013,  Stefan Lundgren,  onehourgroup.com  
//

// This is the engine object render core.
function laRenderAllObjects()
{
	var objectContext = laCurrentObjectContext.renderObjects;
	var length  = objectContext.length;
	var drawLayer = laDrawLayer;

	// Render all objects	
	for(var	layerIndex = 0; layerIndex < length; layerIndex++)
		drawLayer( objectContext[ layerIndex ] );
}


// Add render function to object. 
function laAddRenderFunction( object, key, func )
{
	if( !('render' in object) )
	{
		object.render = {};
		if( !object.child )
			_laAddToRenderList( object, object.layerId );
	}
		
	object.render[key] = func;
}

// Remove render function to object. 
function laRemoveRenderFunction( object, key )
{
	if( 'render' in object )
		delete 	object.render[key];
}


// Draws an object to context, uses cache if available.
function laDrawObject( object, context, isChild )
{
	if( 'cacheCanvas' in object )
	{
		if( object.preRenderRefresh )
			laPreRender( object );
			
		if( isChild == null || !isChild ) 
			_laDrawPreRenderedObject( object, context, laGlobalScale );	
		else
			_laDrawPreRenderedObject( object, context, 1 );		
	}		    
	else 
	{
		if( isChild == null )
			isChild = false;
			
		object.laRenderObject( context, isChild );
	}	
}


// Calls all render callbacks for an object.
function laRenderObject( object, context, isChild )
{
	if( isChild == null )
		isChild = false;

	var globalScale     = 1;
		
	if( !isChild ) 
		globalScale     = laGlobalScale;
		
	var storeContext	= false;
	
	var x               = 0;
	var y               = 0;
	
	var rotation		= 0;
	var	alpha           = 1;
	var scale			= 1;

	var preScaled       = false;
	
	if( 'scaleCanvas' in object )
		preScaled       = true;


    if( 'position' in object )
	{
		if( 'offset' in object )
		{
			x = object.position.x + object.offset.x;
			y = object.position.y + object.offset.y;
		}
		else
		{
			x = object.position.x;
			y = object.position.y;
		}
		
		if( x != 0 || y != 0 )
		{
			storeContext = true;
			// Object and child positions should always be scaled.
			x = Math.round( x * laGlobalScale );
			y = Math.round( y * laGlobalScale );
		}
	}
	
	if( 'rotation' in object )
	{
		// TODO, Do not store context if not rotated. also below.
		rotation		= object.rotation.angle;
		storeContext	= true;
	}
	
	if( 'alpha' in object )
	{
		alpha			= object.alpha;
		storeContext	= true;
	}

	if( !preScaled )
	{
		if( 'scale' in object )
		{
			// No global scale on children. Context already prepared by parent.
			scale = object.scale * globalScale;
				
			storeContext	= true;
		}
		else if( globalScale != 1 )
		{
			scale           = globalScale;
			storeContext	= true;
		}	
	}
	
	
	if( storeContext )
	{
		context.save();
		
		if( x != 0 || y != 0 )
			context.translate( x, y );
		
		if( rotation != 0 )
			context.rotate( rotation );	
		
		if( alpha != 1 )
			context.globalAlpha = alpha;	
			
		if( !preScaled && scale != 1 )
		{
			// Note if scaling and drawing image to cover screen a small increment helps scaling not to miss one line of pixels.
			if( 'boundary' in object && object.boundary.width  == laEngineResolutionWidth && 
				object.boundary.height == laEngineResolutionHeight )
				context.scale( scale+0.01, scale+0.01 );
			else
			context.scale( scale, scale );
		}
	}

	// Render me
	if( 'render' in object )
	{
		for( r in object.render )
		{
			object.render[r]( object, context, preScaled );
		}
	} 

	if( storeContext )
		context.restore();
}




function laRenderLayer( layer, context )
{
	var object = {};
	var func = laDrawObject;
	
	if( context == null )
		context = layer.context;
		
	for(var	i =	0; i < layer.objectCount; i++)
	{		
		object = layer.objects[i];

		if( object.remove || !object.visible )
		{
			laRemoveListObject( layer, i-- );
			continue;
		}

		func( object, context );		
	}
}


// Returns a new layer
function laCreateLayer()
{
	return {objects:[], objectCount:0, remove:false, active:true, preRenderRefresh:false, context:laDefaultContext, clearBackground:true };
}


// Flags an entire layer for removal.
function laRemoveLayer( id )
{
	var layer = laGetLayer( id )
	
	if( layer != null )
		layer.remove = true;
}

// get layer object from id.
function laGetLayer( layerId )
{
	if( layerId < laCurrentObjectContext.renderObjects.length )
	{
		return laCurrentObjectContext.renderObjects[ layerId ];
	}
	
	return null;
}


// Clears the context of a layer.
function laClearLayer( layer )
{
	if( layer == null )
		layer = 0;

	laCurrentObjectContext.renderObjects[layer].context.clearRect (0,0,laCanvasWidth,laCanvasHeight);
}



// Process all objects in layer.
function laDrawLayer( layer )
{
	// Rerender pre rendered layer
	if( layer.preRenderRefresh )
		laPreRenderLayer( layer );
	
	// If layer pre rendered just draw the pre rendered canvas	
	if( 'cacheCanvas' in layer && layer.cacheCanvas.width > 0 && layer.cacheCanvas.height > 0 )
		layer.context.drawImage(layer.cacheCanvas, 0, 0);
	else if( 'canvas' in layer && layer.canvas.width > 0 && layer.canvas.height > 0 )
		laLog ("Nothing to render " + layer.canvas.style.zIndex );
	else
		laRenderLayer( layer );	
}




// Flag object for refresh of pre rendering cache.
function laRefreshPreRender( object )
{
	object.preRenderRefresh = true;
}

// Pre render object to canvas cache.
function laPreRender( object )
{
	if( !( 'cacheCanvas' in object) )
		return;
		
	var cc = object.cacheCanvas;
	
	// Unflag object for refresh
	object.preRenderRefresh = false;

	var cacheContext = cc.getContext('2d');
	
	// Store object position
	var x = object.position.x;
	var y = object.position.y;
	
	// Clear cache context
	cacheContext.clearRect (0,0,cc.width,cc.height);
	
	var alignment = { type:object.alignment.type, x:0, y:0 };
	
	_laUpdateAlignment( alignment, -cc.width/laGlobalScale, -cc.height/laGlobalScale );
			
	// using object alignment when rendering object to cache
	laSetPositionXY( object, alignment.x, alignment.y);
	
	// pre render object to cache.
	object.laRenderObject( cacheContext );
	
	// restore object position
	laSetPositionXY( object, x, y );
}


// Apply prerendering to object using canvas cache with given width and height
// 'full' applies a margin to the cache canvas enough to rotate the object 360 without clipping.
// 'exact' use exact boundary of object but scaled.
function laSetPreRender( object, cacheType )
{
	if( cacheType == null )
	{
		if( !('cacheType' in object ) )
			cacheType = 'full';
		else
			cacheType = object.cacheType;
	}	
		
	object.cacheType = cacheType;
	
	var canvas    = null;
	
	// Reuse cachecanvas when resize.
	if( 'cacheCanvas' in object )
		canvas = object.cacheCanvas;
	else		
		canvas = document.createElement('canvas');

	if( cacheType == 'full' )
	{
		canvas.width  = Math.round( Math.max( object.boundary.width, object.boundary.height ) * Math.SQRT2 * laGlobalScale);
		canvas.height = Math.round( Math.max( object.boundary.width, object.boundary.height ) * Math.SQRT2 * laGlobalScale);
		canvas.style.width = canvas.width + "px";
		canvas.style.height = canvas.height + "px";		
	}
	else if( cacheType == 'exact')
	{
		canvas.width  = Math.round( object.boundary.width  * laGlobalScale);
		canvas.height = Math.round( object.boundary.height * laGlobalScale);	
		canvas.style.width = canvas.width + "px";
		canvas.style.height = canvas.height + "px";	
	}
	
	object.cacheCanvas = canvas;
	

	laRefreshPreRender( object );
}


function laRemovePreRender( object )
{
	if( 'cacheType' in object )
		delete object.cacheType;
		
	if( 'cacheCanvas' in object )
		delete object.cacheCanvas;		
}

// Apply prescaling of graphics to improve performance on all resolutions.
function laSetPreScale( object )
{
	var canvas    = null;
	
		// Reuse scalecanvas when resize.
	if( 'scaleCanvas' in object )
		canvas = object.scaleCanvas;
	else		
		canvas = document.createElement('canvas');

	canvas.width  = Math.round( object.boundary.width  * laGlobalScale );
	canvas.height = Math.round( object.boundary.height * laGlobalScale );
	canvas.style.width = canvas.width + "px";
    canvas.style.height = canvas.height + "px";
	
	//laLog ("Prescale canvas " + canvas.width + " x " + canvas.height );
	
	object.scaleCanvas  = canvas;
	
	laPreScale( object );
}

function laRemovePreScale( object )
{
	if( 'scaleCanvas' in object )
		delete object.scaleCanvas;
		
}

function laPreScale( object )
{
	if( !( 'scaleCanvas' in object) )
		return;
		
	if( !( 'image' in object) )
		return;
		
	if( !( 'position' in object) )
		return;
		
	var scaleContext = object.scaleCanvas.getContext('2d');
	
	// Clear cache context
	scaleContext.clearRect (0,0,object.scaleCanvas.width, object.scaleCanvas.height);
	
	scaleContext.scale( laGlobalScale, laGlobalScale );
		
	scaleContext.drawImage( object.image, 0, 0 );
}

// Flag layer for refresh of pre render cache
function laRefreshPreRenderLayer( layer )
{
	layer.preRenderRefresh = true;
}


// Pre render layer to cache
function laPreRenderLayer( layer )
{
	if( !layer.preRenderRefresh )
		return;

	var context = null;		
	if( 'cacheCanvas' in layer ) 
	{
		context = layer.cacheCanvas.getContext('2d');
		context.clearRect (0,0,layer.cacheCanvas.width,layer.cacheCanvas.height);
	}	
	else if( 'canvas' in layer ) 
	{
		context = layer.canvas.getContext('2d');
		context.clearRect (0,0,layer.canvas.width,layer.canvas.height);
	}		
	else
	{
		context = layer.context;
		context.clearRect (0,0,laCanvasWidth, laCanvasHeight );
	}
	
	if( context == null )
		return;

	laRenderLayer( layer, context );	

	layer.preRenderRefresh = false;
}



// Apply pre rendering to layer. Render layer to offscreen canvas and draw whole canvas att render time.
function laSetPreRenderLayer( layerId, cacheCanvas )
{
	if( cacheCanvas == null )
		cacheCanvas = document.createElement('canvas');

	cacheCanvas.width     = laCanvasWidth;
	cacheCanvas.height    = laCanvasHeight;

	var layer             = laGetLayer( layerId );
	
	if( layer )
	{
		layer.cacheCanvas = cacheCanvas;
		laRefreshPreRenderLayer( layer );
	}
}


// Add z indexed canvas for layer into the document.
function laSetCanvasLayer( layerId, canvas, zIndex )
{
	if( canvas == null )
	{
		canvas = document.createElement('canvas');
		document.body.appendChild(canvas);
		laHandleInputEvents( canvas );
	}

	canvas.height         = laCanvasHeight; //window.screen.height;
	canvas.width          = laCanvasWidth;  //window.screen.width;		
	canvas.style.position = 'fixed';
	canvas.style.top      = laMarginTop + 'px';
	canvas.style.left     = laMarginLeft + 'px';
	canvas.style.zIndex   = zIndex;

	var layer             = laGetLayer( layerId );
	

	if( layer )
	{
		layer.canvas = canvas;
		laRefreshPreRenderLayer( layer );
	}
}


//
// Engine internal functions:
//

function _laResizeObject( object, context )
{
	laCalcBoundary( object );
	
	if( 'cacheCanvas' in object )
		laSetPreRender( object );
		
	if( 'scaleCanvas' in object )
		laSetPreScale( object );
		
	if( 'childObjects' in object )
	{
		var objects = object.childObjects;
		for( var i=0; i < objects.length; i++ )
		{
			_laResizeObject( objects[i], context );
		}
	}		
}

function _laResizeLayer( layerId )
{
	var layer = laGetLayer( layerId )
	
	if( 'cacheCanvas' in layer )
	{
		laSetPreRenderLayer( layerId, layer.cacheCanvas );
		_laForEachLayerObject( layer, layer.cacheCanvas.getContext('2d'), _laResizeObject );
	}
	else if( 'canvas' in layer )
	{
		laSetCanvasLayer( layerId, layer.canvas, layerId );
		_laForEachLayerObject( layer, layer.canvas.getContext('2d'), _laResizeObject );
	}
	else
		_laForEachLayerObject( layer, layer.context, _laResizeObject );
}


function _laResizeObjectContext()
{
	var elem = (document.compatMode === "CSS1Compat") ? 
		document.documentElement : document.body;

	var height			= elem.clientHeight;
	var width			= elem.clientWidth;
 
	var resizeObjects	= false;
	
	xScale				= width  / laEngineResolutionWidth;
	yScale				= height / laEngineResolutionHeight;
	
	var scale			= "auto";
	
	if( laResizeStyle == "auto" )
		scale			= Math.min( xScale, yScale );
	else if (laResizeStyle == "cover")
		scale			= Math.max( xScale, yScale );
	else if (laResizeStyle == "width")
		scale			= xScale;
	else if (laResizeStyle == "height")
		scale			= yScale;	
	
	var canvasWidth   = Math.round(scale*laEngineResolutionWidth );
	var canvasHeight  = Math.round(scale*laEngineResolutionHeight);


	if( laCanvasAlignment.type.indexOf("left") != -1 )
		laMarginLeft = 0;
	else if( laCanvasAlignment.type.indexOf("right") != -1)
		laMarginLeft = Math.round((width  - canvasWidth));
	else if( laCanvasAlignment.type.indexOf("-center") != -1)
		laMarginLeft = Math.round((width  - canvasWidth) /2);;
	
	if( laCanvasAlignment.type.indexOf("top") != -1 )
		laMarginTop = 0;	
	else if( laCanvasAlignment.type.indexOf("bottom") != -1 )
		laMarginTop = Math.round((height - canvasHeight));		
	else if ( laCanvasAlignment.type.indexOf("center-") != -1 )
		laMarginTop = Math.round((height - canvasHeight)/2);

	
	
	if ( laResizeByStyle )
	{
		laBrowserScale = scale;
		laDefaultCanvas.setAttribute('style', 'width:'+canvasWidth+'px;height:'+canvasHeight+'px');	
		laDefaultCanvas.style.position = 'fixed';
		laDefaultCanvas.style.top      = laMarginTop  + 'px';
		laDefaultCanvas.style.left     = laMarginLeft + 'px';
		laDefaultCanvas.style.zIndex   = "0";		
		
		var xScale        = laCanvasWidth  / laEngineResolutionWidth;
		var yScale        = laCanvasHeight / laEngineResolutionHeight;	
		laGlobalScale = Math.min( xScale, yScale );	
	}
	else
	{
		laGlobalScale  = scale;
		laCanvasWidth  = canvasWidth;
		laCanvasHeight = canvasHeight;		

		laDefaultCanvas.height         = laCanvasHeight; 		 //window.screen.height;
		laDefaultCanvas.width          = laCanvasWidth;  		 //window.screen.width;		
		laDefaultCanvas.style.position = 'fixed';
		laDefaultCanvas.style.top      = laMarginTop  + 'px';
		laDefaultCanvas.style.left     = laMarginLeft + 'px';
		laDefaultCanvas.style.zIndex   = "0";
		laDefaultCanvas.style.height   = laCanvasHeight + 'px';  //window.screen.height;
		laDefaultCanvas.style.width    = laCanvasWidth  + 'px';  //window.screen.width;		
		
		_laForEachLayerId( _laResizeLayer );
		
		_laResizeDebug();
	}
}


// Calls all objects in layer usinf function callback
function _laForEachLayerObject( layer, context, callback )
{
	for(var	i =	0; i < layer.objectCount; i++)
	{
		var object = layer.objects[i];
		callback( object, context );
	}	
}


// Apply callback for each layer include parameter context..
function _laForEachLayer( callback, currentObjectContext )
{
	var length  = currentObjectContext.length;
	
	for(var	layerIndex = 0; layerIndex < length; layerIndex++)
	{
		callback( currentObjectContext[ layerIndex ] );	
	}
}


// Apply callback for each layer include parameter context..
function _laForEachLayerId( callback )
{
	var length = laCurrentObjectContext.renderObjects.length;
	for(var	layerIndex = 0; layerIndex < length; layerIndex++)
	{
		callback( layerIndex );	
	}
}



function _laAddToRenderList( object, layerId )
{
	var renderObjects = laCurrentObjectContext.renderObjects;	
	
	if( renderObjects.length <= layerId )
		for(var	i =	renderObjects.length; i <= layerId; i++)
			renderObjects[i] = laCreateLayer();
	
	if( object.visible && 'render' in object && object.render.length != 0 )
	{
		var layer = renderObjects[layerId];
	
		layer.objectCount++;
		layer.objects[layer.objectCount-1] = object;
	}
}

// TODO do not like param globalDScale
function _laDrawPreRenderedObject( object,  context, globalScale )
{
	// Just draw the pre rendered object.
	// TODO: hantera object som har individuell skalning.
	var cc = object.cacheCanvas;
	
	// Alignment works only if object boundary is exact with cachecanvas size
	if( object.cacheType == "exact" )
		context.drawImage(cc,	Math.round( globalScale * (object.position.x + object.alignment.x) ), 
								Math.round( globalScale * (object.position.y + object.alignment.y) ) );
	else
		context.drawImage(cc,	Math.round( globalScale * object.position.x - cc.width/2), 
								Math.round( globalScale * object.position.y - cc.height/2) );
	
	//laLog( cc.width + " x " + cc.height );
	
	if( laDbgMode == laDebugModeEx )
	{
		context.save()
		context.globalAlpha = 0.2;
		context.fillStyle = "#00c";
		context.fillRect (	Math.round( globalScale * object.position.x - cc.width/2), 
							Math.round( globalScale * object.position.y - cc.height/2),
							Math.round( cc.width), 
							Math.round( cc.height) );
		context.restore()
	}
}

// draw prerendered layer.
function _laDrawPreRenderedLayer( layerCanvas, context )
{
	// Just draw the pre rendered layer.
	context.drawImage(layerCanvas, 0, 0);
}
