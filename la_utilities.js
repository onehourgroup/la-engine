//
// Copyright 2013,  Stefan Lundgren,  onehourgroup.com  
//

function isMobile()
{
	//Initialize our user agent string to lower case.
	var uagent = navigator.userAgent.toLowerCase();
	
	if ( uagent == "android" )
		return true;
		
	return false;
}



function roundNumber(num, dec) 
{
	var result = Math.round(num*Math.pow(10,dec))/Math.pow(10,dec);
	return result;
}


function laGetGridXY( posX, posY )
{
	return { x:Math.floor( posX / laGridSize), y:Math.floor(posY / laGridSize) };
}

function laGetSnapPosition( position )
{
    return {x:Math.round(position.x - position.x % laGridSize + laGridSize/2), 
			y:Math.round(position.y - position.y % laGridSize + laGridSize/2)};
}

