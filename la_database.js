var laCordovaFilename = "";
var laCordovaFolder = "";

var laCordovaDatabase = {};

var laReadRequest = false;
var laWriteRequest = false;

function laInitDb(filename, folder)
{
	if( window.cordova )
	{
		laCordovaFilename = filename;
		
		if(folder != null)
			laCordovaFolder = folder;
	}	
}



function _gotFS(fileSystem) 
{
	if(laCordovaFolder != "" )
		fileSystem.root.getDirectory(laCordovaFolder, {create: true, exclusive: false}, _gotDirEntry, _failFS);
	else
		fileSystem.root.getFile(laCordovaFilename, {create: true}, _gotFileEntry, _failFS);	
}


function _gotDirEntry(dirEntry) 
{
	dirEntry.getFile(laCordovaFilename, {create: true}, _gotFileEntry, _failFS);
}

function _gotFileEntry(fileEntry) 
{
	if(laWriteRequest)
    {
		fileEntry.createWriter(_gotFileWriter, _failFS);     
    }	
    else
		fileEntry.file(_gotFile, _failFS);
}

function _gotFile(file){
	alert("Got File " + laWriteRequest + laReadRequest);
	
	var reader = new FileReader();
	reader.onloadend = function(evt) {
		alert(evt.target.result);
		laCordovaDatabase = JSON.parse(evt.target.result)
	};
	reader.readAsText(file);
	
	laReadRequest = false;
	laWriteRequest = false;			
}

function _gotFileWriter(writer)
{
    writer.onwriteend = function(evt) 
    {
	    alert('file written');
	    
		laReadRequest = false;
		laWriteRequest = false;
    };

    writer.write(JSON.stringify(laCordovaDatabase)); //function variable of fileSys();
}


function _failFS(evt) {
    alert(evt.target.error.code);
    
	laReadRequest = false;
	laWriteRequest = false;
}



function laSaveItem( key, value )
{
	// Check for phonegap/cordova
	if( window.cordova )
	{
		laWriteRequest = true;
		laCordovaDatabase[key]=value;
		alert("Writing " + laCordovaDatabase);
		alert(JSON.stringify(laCordovaDatabase));
		window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, _gotFS, _failFS);
	}
	else
	{
		localStorage.setItem(key, JSON.stringify(value));
	}
}

function laLoadItem(key)
{
	var value = null;
	
	if( window.cordova )
	{
		laReadRequest = true;
		window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, _gotFS, _failFS);
		
		if( !(key in laCordovaDatabase) )
			value = null;
		else
			value = laCordovaDatabase[key];
	}
	else
	{
		value = localStorage.getItem(key);
	}
	
	if ( value == 'undefined' || typeof value == 'undefined' || value == null )
		return null;
					
	return JSON.parse(value);
}

function laSaveLocalStorageItem( key, value )
{
	localStorage.setItem(key, JSON.stringify(value));
}

function laLoadLocalStorageItem(key)
{
	var value = localStorage.getItem(key);
	
	if ( value == 'undefined' || typeof value == 'undefined' || value == null )
		return null;
		
	return JSON.parse(value);
}

