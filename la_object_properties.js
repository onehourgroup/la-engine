//
// Copyright 2013,  Stefan Lundgren,  onehourgroup.com  
//

function laInactivate( object )
{
	object.active = false;
}

function laActivate( object )
{
	if( object.active )
		return;
		
	object.active = true;
	_laAddToUpdateList(object);
}


function laShow( object )
{
	if( object.visible )
		return;
		
	object.visible = true;
	_laAddToRenderList(object, object.layerId);
}

function laHide( object )
{
	object.visible = false;
}

// set position as reference to position object
function laSetPosition( object, pos )
{
	object.position	  = pos;
}

// update objects position x and y coordinate.
function laSetPositionXY( object, x, y )
{
	object.position   = {x:x, y:y};
}

function _laUpdateAlignment( alignment, width, height)
{
	if( alignment.type.indexOf("left") != -1 )
		alignment.x = 0;
	else if( alignment.type.indexOf("right") != -1)
		alignment.x = -width;
	else if( alignment.type.indexOf("-center") != -1)
		alignment.x = -Math.round( width/2 );
	
	if( alignment.type.indexOf("top") != -1 )
		alignment.y = 0;	
	else if( alignment.type.indexOf("bottom") != -1 )
		alignment.y = -height;		
	else if ( alignment.type.indexOf("center-") != -1 )
		alignment.y = -Math.round( height/2 );
}

// TODO: this function only works if x y = 0;
function laSetObjectAlignment( object, type )
{
	if( !('boundary' in object ) )
		laCalcBoundary( object );
	
	if( type == null || type == "center")
		type = "center-center";
		
	object.alignment = {type:type, x:0, y:0};
	
	_laUpdateAlignment( object.alignment, object.boundary.width, object.boundary.height);
}


function laSetPositionOffsetXY( object, x, y )
{
	object.offset			= {};
	object.offset.x			= x;
	object.offset.y			= y;
	object.offset.rotation	= 0;	
}

function laSetRotationData( object, angle, rotationSpeed )
{
	object.rotation = {};
	object.rotation.angle = angle;
	object.rotation.speed = rotationSpeed;
}


// Set the speed of an object. [ pixels / frame ]
function laSetSpeed( object, speed )
{
	object.speed      = speed;
	object.speedCount = 0.0;
}



// Add target to en of target list
function laAddTarget( object, target, updateFunc )
{
	if( updateFunc == null )
		updateFunc = laUpdatePositionAndRotationToTarget;
	
	if( !('targets' in object) )
		object.targets = [];
		
	object.targets[object.targets.length]	= target;
	object.count							= {x:0, y:0};
	
	laAddUpdateFunction( object, 'update', updateFunc );
	laSetRotationData( object, 0, 0 );	
}

// Reset to one and only target.
function laSetTarget( object, target, updateFunc )
{
	object.targets	= [];
	
	laAddTarget( object, target, updateFunc );
}

// Set target list
function laSetTargets( object, targets, updateFunc )
{
	if( updateFunc == null )
		updateFunc = laUpdatePositionAndRotationToTarget;
		
	object.targets	= targets;
	object.count	= {x:0, y:0};
	
	laAddUpdateFunction( object, 'update', updateFunc );	
	laSetRotationData( object, 0, 0 );		
}

// Makes an object stop and turn against next target in each checkpoint.
function laSetTargetStopTurn( object, stopTurn )
{
	object.stopTurn = true;
}


// Pause updates for object in millisec milliseconds.
function laSetDelayedUpdate( object, millisec )
{
	object.delayFrames = Math.round( millisec / laUpdateDelay );
	
	if( object.delayFrames <= 0 )
		object.delayFrames = 1;
}



function laSetTimer( object, millisec, callback )
{
	object.timer = {frameCount:Math.round(millisec / laUpdateDelay), callback:callback };
	
	if( object.timer.frameCount == 0 )
		object.timer.frameCount = 1;

	laAddUpdateFunction( object, 'timer', laCheckTimer );		
}

function laSetFlash( object, activeMs, inactiveMs )
{
	object.flash = {activeTime:activeMs, inactiveTime:inactiveMs};
	laToggleFlash( object )
}

function laSetBackgroundFromPath( object, imagePath, type )
{
	var image = new Image();
	image.src = imagePath;
	laSetBackground( object, image, type );
}

function laSetBackground( object, image, type )
{
	if( type == null )
		type = "None";
		
	
	//if( type == "Fit" )
	//{
		//laEngineResolutionWidth  = image.width;
		//laEngineResolutionHeight = image.height;
	//}
	
	laSetPositionXY( object, Math.round(laEngineResolutionWidth/2), Math.round(laEngineResolutionHeight/2) );
	
	laSetImage( object, image );
}

function laSetImageFromPath( object, imagePath )
{
	var image = new Image();
	image.src = imagePath;
	
	laSetImage( object, image );
}

function laSetImage( object, image )
{
	if( 'image' in object )
	{
		delete object.image;
	}
	
	object.image = image;
	laCalcBoundary( object );
	
	laAddRenderFunction( object, 'image', laRenderImage );	
	
	laSetPreScale( object );
}

function  laSetBoundary( object, width, height )
{
	object.boundary = { width:width, height:height};
}

function laBoundaryOk( object ) 
{
	if( !('boundary' in object ) )
		return false;
		
	if( 'image' in object )
	{
		return (object.boundary.width == object.image.width ) &&
		(object.boundary.height == object.image.height);
	}

	if( 'dialog' in object )
	{
		return (object.boundary.width == object.dialog.width ) &&
		(object.boundary.height == object.dialog.height);
	}

	return false;		
}

function laCalcBoundary( object )
{
	if( !('boundary' in object ) )
	{
		laSetBoundary( object, 0, 0 );
	}
	
	if( 'image' in object )
	{
		object.boundary.width  = object.image.width;
		object.boundary.height = object.image.height;
	}

	if( 'dialog' in object )
	{
		if('talkTarget' in object)
		{
			object.boundary.width  = Math.round(object.dialog.width*1.2);
			object.boundary.height = Math.round(object.dialog.height*1.2);
		}
		else
		{
			object.boundary.width  = object.dialog.width;
			object.boundary.height = object.dialog.height;
		}
	}
	
	if( 'text' in object )
	{
		if( 'font' in object )
		{
			var letterWidth  = object.font.letterWidth;
			var letterHeight = object.font.letterHeight;
			
			object.boundary.width = object.text.length * letterWidth;
			object.boundary.height = letterHeight;
		}
	}

	if( 'scale' in object )
	{
		object.boundary.width  = Math.round( object.boundary.width  * object.scale );
		object.boundary.height = Math.round( object.boundary.height * object.scale );
	}
	else
	{
		object.boundary.width  = Math.round( object.boundary.width  );
		object.boundary.height = Math.round( object.boundary.height ); 
	}
	
	if( !('alignment' in object) )
		laSetObjectAlignment( object, "center" );
}

function laSetScale( object, scale )
{
	object.scale = scale;
	laCalcBoundary( object );
}




function laAnimateImages( object )
{
	
	var timeLeft = object.images[object.currentImage].timeLeft;
	
	if( timeLeft == 0 )
	{
		if( object.images.length - 1 > object.currentImage )
		{
			laSetAnimate( object, object.currentImage + 1);
		}
		else
		{
			if( ('removeComplete' in object) && object.removeComplete )
			{
				laRemoveObject(object);
			}
			else if( 'animateLoop' in object )
			{
				laSetAnimate( object, 0 );
			}
		}
	}
	else
		object.images[object.currentImage].timeLeft--;
	
}

function laAnimateLoop( object )
{
	object.animateLoop = true
}

function laAddImage( object, image, showFrames )
{
	if( !('images' in object) )
	{
		object.images = [];
		
		laSetImage( object, image );
		laAddUpdateFunction( object, 'animate', laAnimateImages );
	}
	
	if( object.images.length == 0 )
		object.currentImage = 0;
	
	object.images[object.images.length] = {image:image, imageShowTime:showFrames, timeLeft:showFrames };
}


function laAddImageFromPath( object, imagePath, showFrames )
{
	var image = new Image();
	image.src = imagePath;
	
	laAddImage( object, image, showFrames );

}

function laSetAnimate( object, index )
{
	object.currentImage = index;
	object.images[object.currentImage].timeLeft = object.images[object.currentImage].imageShowTime;
	
	laSetImage( object, object.images[object.currentImage].image );
	
}


function laRemoveAfterAnimate( object, removeComplete )
{
	if (removeComplete == null )
		object.removeComplete = false;
	else
		object.removeComplete = removeComplete;
}

function laSetAlpha( object, alpha )
{
	object.alpha = alpha;
}


function laSetRange( objectType, range )
{
	objectType.range = range;
	laAddRenderFunction( objectType, 'range', laRenderRange );		
}

function laSetSelectable( object, selected )
{
	laAddRenderFunction( object, 'selected', laRenderSelected);
	laSetSelected( object, selected );	
}

function laSetSelectableGrid( object, selected )
{
	laAddRenderFunction( object, 'selected', laRenderSelectedGrid );
	laSetSelected( object, selected );	
}

function laCreateButton( x, y, image, callback, layerId )
{
	if( layerId == null )
		layerId = 0;
		
	var button = laCreateObject(layerId);
	
	laSetPositionXY( button, x, y);
	laSetImage(button, image);
	laSetMouseClickAction( button, callback );
	
	return button;
}

function laSetSelected( object, selected )
{
	var performCallback = false;
	
	if( !('selected' in object) )
	{
		object.selected = false;
	}
	
	if(	object.selected != selected )
	{
		performCallback = true;
	}
		
	object.selected = selected;	
	
	if( 'selectedCallback' in object && performCallback )
		object.selectedCallback( object, object.selected );
}

function laSelected( object )
{
	if( !('selected' in object) )
		return false;
	
	return object.selected;	
}

function laFade( object, startAlpha, targetAlpha, alphaSpeed, fadeCompleteCB )
{
	laAddUpdateFunction( object, 'fade', laUpdateFade );
	
	object.alpha				= startAlpha;
	object.alphaSpeed			= alphaSpeed;
	object.targetAlpha  		= targetAlpha;
	
	object.fadeCompleteCallback	= fadeCompleteCB;
}

function laFadeIn( fadeCompleteCB )
{
	var object = laCreateObject( laCurrentObjectContext.renderObjects.length );
	
	laSetBoundary( object, laEngineResolutionWidth, laEngineResolutionHeight );
	
	laSetObjectAlignment( object, "top-left" );

	laAddRenderFunction( object, 'fade', laRenderFade );

	laFade( object, 1, 0, -0.01, fadeCompleteCB );	
}

function laFadeOut( fadeCompleteCB )
{
	var object = laCreateObject( laCurrentObjectContext.renderObjects.length );
	
	laSetBoundary( object, laEngineResolutionWidth, laEngineResolutionHeight );
	
	laSetObjectAlignment( object, "top-left" );

	laAddRenderFunction( object, 'fade', laRenderFade );

	laFade( object, 0, 1, 0.01, fadeCompleteCB );	
}

function laUpdateFade( object )
{
	object.alpha	   += object.alphaSpeed; 

	if( object.alpha < 0 )
		object.alpha = 0;
		
	if( object.alpha > 1 )
		object.alpha = 1;
		
	if( object.alpha <= object.targetAlpha && object.alphaSpeed < 0 || object.alpha >= object.targetAlpha && object.alphaSpeed > 0 )
	{
		laRemoveLayer( object.layerId );
		laRemoveObject(object);
		if( object.fadeCompleteCallback )
		{
			object.fadeCompleteCallback();
			object.fadeCompleteCallback = null;
		}
	}	
}

function laRenderFade( object, context )
{
	context.save()
	context.fillStyle	= "#000";

	context.globalAlpha = object.alpha;
	context.fillRect (object.alignment.x, object.alignment.y, object.boundary.width, object.boundary.height);
	context.restore();	
}

function laTextRow( object, font, text )
{
	object.text = text;
	object.font = font;
	laAddRenderFunction( object, 'text', laRenderText );
	
	laSetObjectAlignment( object, 'top-left' );
	
	laCalcBoundary(object);
}

function laSetTextDialog( object, width, height, text, font )
{
	object.dialog = {};
	object.dialog.text   = text;
	object.dialog.width  = width;
	object.dialog.height = height;
	object.dialog.font   = font;
	
	laSetObjectAlignment( object, "center-center");
	laCalcBoundary( object );
	laAddRenderFunction( object, 'dialog', laRenderDialog );
	
	laSetPreRender( object, 'exact' );
	
	return object;
}


function laSetTalkDialog( object, width, height, text, font )
{
	var dialog = laSetTextDialog( object, width, height, text, font );
	
	var talkArrow = laCreateObject( object.layerId );
	
	laSetPosition( talkArrow, dialog.position );
	
	talkArrow.dialog = dialog;
	
	laAddRenderFunction( talkArrow, 'talkArrow', laRenderTalkArrow );
}