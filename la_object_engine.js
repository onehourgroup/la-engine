//
// Copyright 2013,  Stefan Lundgren,  onehourgroup.com  
//

// Engine global variables
var laInitReadyCallback             = null;
var laEngineStartTime               = null;
var laIntervalId					= 0;

// Screen updating
var laEngineFramesPerSecond			= 60;
var laEngineFrameDelay				= Math.round(1000 / laEngineFramesPerSecond);
var laEngineFrameCount         		= 0;

// Object updating
var laUpdatesPerSecond              = 60;
var laUpdateDelay					= Math.round(1000 / laUpdatesPerSecond);
var laEngineUpdateCount             = 0;
var laEngineUpdateSpeed				= 1;


// The visible HTML canvas object.
var laCanvasStack                   = [];
var laDefaultCanvas                 = null;
var laDefaultContext				= null;

// Visible canvas resolution.
var	laCanvasWidth                   = 800;
var	laCanvasHeight                  = 480;	

// Internal resolution. All objects use internal coordinates.
var laEngineResolutionWidth         = 800;
var laEngineResolutionHeight        = 480;

// Decides if canvas should resize to screen resolution. 
var laAutoResize                    = true;
var laResizeStyle					= "auto";  // auto, width, height, cover
var laResizeByStyle                 = false;
var laGlobalScale					= 1;
var laBrowserScale                  = 1;
var laGridSize						= 48;


var laCanvasAlignment               = {type:"center-center", x:0, y:0};
// Marigins used around canvas.
var laMarginLeft                    = 0;
var laMarginTop                     = 0;


// Engine object contexts and containers.
var laObjectContext			        = {};
var laCurrentObjectContext	        = null;
var laCurrentObjectContextId        = null;


// Enginde debug variables
var laDbgMode						= 0;

function laClone( object ) {

  var newObj = (object instanceof Array) ? [] : {};
  
  for (i in object) 
  {
    propRef = object[i];
    if (propRef && typeof propRef == "object" && i != "cacheCanvas" && i != "scaleCanvas" && i != "image")
		newObj[i] = laClone(propRef);
    else 
		newObj[i] = propRef;
  } 

  return newObj;
};

function laCopy( srcObject, dstObject ) {
 
  for (var i in srcObject) 
  {
    var propRef = srcObject[i];
    if (propRef && typeof propRef == "object" && i != "cacheCanvas" && i != "scaleCanvas" && i != "image")
    {
		dstObject[i] = (propRef instanceof Array) ? [] : {};
		laCopy(propRef, dstObject[i]);
	}
    else 
		dstObject[i] = propRef;
  } 
};

// When ready...
window.addEventListener("load",function() {
  // Set a timeout...
  setTimeout(function(){
    // Hide the address bar!
    window.scrollTo(0, 1);
  }, 0);
});


var PIXEL_RATIO = (function () {
    var ctx = document.createElement("canvas").getContext("2d"),
        dpr = window.devicePixelRatio || 1,
        bsr = ctx.webkitBackingStorePixelRatio ||
              ctx.mozBackingStorePixelRatio ||
              ctx.msBackingStorePixelRatio ||
              ctx.oBackingStorePixelRatio ||
              ctx.backingStorePixelRatio || 1;

    return dpr / bsr;
})();


// Adds a canvas into the HTML document
function laAddCanvas( canvasId, zIndex, width, height, marginLeft, marginTop )
{
	if( width == null )
		width  = laCanvasWidth;
		
	if( height == null )
		height = laCanvasHeight;
		
	if( marginLeft == null )
		marginLeft = laMarginLeft;
		
	if( marginTop == null )
		marginTop = laMarginTop;
		
	var canvas                     = document.createElement('canvas');	
	laCanvasStack[canvasId]    = canvas;	
	canvas.width               = width; 
	canvas.height              = height; 
	canvas.style.position      = 'fixed';
	canvas.style.backgroundColor = '#000000';
	canvas.style.left          = marginLeft + 'px';
	canvas.style.top           = marginTop + 'px';
	canvas.style.width = width + "px";
    canvas.style.height = height + "px";
	
	/*if ( window.cordova )
	{
		canvas.style.image-rendering = '-webkit-optimize-contrast';
	}*/
	
	if( zIndex != null )
		canvas.style.zIndex    = zIndex;
	
	document.body.appendChild(canvas);
	
	return canvas;
}

// Removes a HTML canvas from document.
function laRemoveCanvas( canvasId )
{
	var canvas = laCanvasStack[canvasId];

	document.body.removeChild(canvas);	
	
	delete laCanvasStack[canvasId];	
}

// Prepare and/or clear engine
function laInitEngine( initReadyCallback, x, y )
{
	if( initReadyCallback == null )
		laLog("Please pass a function to be called when engine is ready.");
		
	laInitReadyCallback = initReadyCallback;
	
	if( x != null )
		laEngineResolutionWidth  = x;
	
	if( y != null )
		laEngineResolutionHeight = y;	
	
	_laInitScript();
}


// Starts the engine object processing
function laStartEngine()
{
	laSetFramesPerSecond( laEngineFramesPerSecond );
	
	laEngineStartTime = new Date().getTime();
}

// Stops the engine object processing
function laStopEngine()
{
	laSetFramesPerSecond( 0 );
}

function laInitObjectContext()
{
	return {renderObjects:[laCreateLayer()], updateObjects:[], events:{}, mouseTouch:{} };
}

// Set active object context.
function laSetObjectContext( id )
{
	if( !( id in laObjectContext ))
	{
		laLog ("New object context " + id);
		laObjectContext[id] = laInitObjectContext();
		
	}
	
	laCurrentObjectContext   = laObjectContext[id];
	laCurrentObjectContextId = id;
}


function _laUpdateObject()
{
	if( this.delayFrames-- > 0 )
		return;

	for( var u in this.update )
		this.update[u]( this );
};


function _laRenderObject( context, isChild )
{
	if( isChild == null )
		isChild = false;

	var globalScale     = 1;
		
	if( !isChild ) 
		globalScale = laGlobalScale;
		
	var storeContext	= false;
	
	var x               = 0;
	var y               = 0;
	
	var rotation		= 0;
	var	alpha           = 1;
	var scale			= 1;

	var preScaled       = false;
	
	if( 'scaleCanvas' in this )
		preScaled = true;


	if( 'position' in this )
	{
		if( 'offset' in this )
		{
			x = this.position.x + this.offset.x;
			y = this.position.y + this.offset.y;
		}
		else
		{
			x = this.position.x;
			y = this.position.y;
		}
		
		if( x != 0 || y != 0 )
		{
			storeContext = true;
			// Object and child positions should always be scaled.
			x = Math.round( x * laGlobalScale );
			y = Math.round( y * laGlobalScale );
		}
	}
	
	if( 'rotation' in this )
	{
		// TODO, Do not store context if not rotated. also below.
		rotation		= this.rotation.angle;
		storeContext	= true;
	}
	
	if( 'alpha' in this )
	{
		alpha			= this.alpha;
		storeContext	= true;
	}

	if( !preScaled )
	{
		if( 'scale' in this )
		{
			// No global scale on children. Context already prepared by parent.
			scale = this.scale * globalScale;
				
			storeContext	= true;
		}
		else if( globalScale != 1 )
		{
			scale           = globalScale;
			storeContext	= true;
		}	
	}
	
	
	if( storeContext )
	{
		context.save();
		
		if( x != 0 || y != 0 )
			context.translate( x, y );
		
		if( rotation != 0 )
			context.rotate( rotation );	
		
		if( alpha != 1 )
			context.globalAlpha = alpha;	
			
		if( !preScaled && scale != 1 )
		{
			// Note if scaling and drawing image to cover screen a small increment helps scaling not to miss one line of pixels.
			if( 'boundary' in this && this.boundary.width  == laEngineResolutionWidth && 
				this.boundary.height == laEngineResolutionHeight )
				context.scale( scale+0.01, scale+0.01 );
			else
			context.scale( scale, scale );
		}
	}

	// Render me
	if( 'render' in this )
	{
		for( var r in this.render )
		{
			this.render[r]( this, context, preScaled );
		}
	} 

	if( storeContext )
		context.restore();
};

// Returns an empty engine object (Not handled by engine)
function laObject()
{
	this.position={x:0,y:0};
	this.rotation=0; 
	this.active=true; 
	this.visible=true; 
	this.remove=false; 
	this.preRenderRefresh=false; 
	this.delayFrames=0; 
	this.layerId=0; 
	this.child=false;

	// Calls all update callbacks for an object.
	this.laUpdateObject = _laUpdateObject; 

	// Calls all render callbacks for an object.
	this.laRenderObject = _laRenderObject;
	
}

// Creates an empty object in the current world.
function laCreateObject( layerId )
{
	return laCreateObjectClone( new laObject(), layerId );
}

// Creates an object copy from any object and inserts into current world.
function laCreateObjectClone( object, layerId )
{
	if( layerId == null )
		layerId = 0;
	
	var cloneObject = new laObject();
	laCopy( object, cloneObject);
	
	cloneObject.layerId = layerId;
	
	_laAddToRenderList( cloneObject, layerId );	
	_laAddToUpdateList( cloneObject );
		
	return cloneObject;
}


// Flags an object for removal. Will be removed at next processing.
function laRemoveObject( object )
{
	object.remove = true;
}

// Removes all objects in current world.
function laRemoveObjects()
{
	laObjectContext[laCurrentObjectContextId] = laInitObjectContext();
	laCurrentObjectContext                    = laObjectContext[laCurrentObjectContextId];
}


// Sets the number of engine updates per second.
function laSetFramesPerSecond( fps )
{
	if( laIntervalId != 0 )
		clearInterval( laIntervalId );
	
	if( fps != 0 )
	{
		laEngineFramesPerSecond = fps;
		laEngineFrameDelay      = Math.round(1000 / laEngineFramesPerSecond);		
		laIntervalId            = setInterval(laProcess, laEngineFrameDelay );
	}
}



// Engine core loop.
function laProcess()
{
	if( laDbgMode )
		laDebugFrameStart();

	laEngineFrameCount++;
	
	laUpdateAllObjects();	   		
		
	laRenderAllObjects();	
			
    if( laDbgMode )
    {
		laDebugDisplayInfo();		
		laDebugFrameEnd();				
	}
}



//
//  Internal functions used by engine:
//

function _laGetFrameProcessTime( lastDelayStart )
{
    // Time	since last rendering
    return new Date().getTime() - lastDelayStart;
}

function _laGetRemainingDelay( targetProcessTime, frameProcessTime )
{
    if (frameProcessTime > targetProcessTime)
        return 0;

    return targetProcessTime - frameProcessTime;
}


function _laInitScript()	
{
	// Timeout to wait for cordova to load, otherwise we cant
	// do a proper check for cordova when loading sounds
	setTimeout(function() {_laInitCheck();},500);
}

function _laInitCheck()
{
	if ( window.cordova )
	{
		laLog ("We are on a mobile device");
		// Wait for phonegap to initialize before starting (no sound otherwise)
		document.addEventListener("deviceready", _laResetEngine, false);
	}
	else
	{
		laLog ("We are in a web browser");
		// No phonegap, start mars defence init directly
		_laResetEngine ();
	}
}

function _laResetEngine()
{	
	laSetObjectContext('default');
	
	laDefaultCanvas     = laAddCanvas( 'default' );
	laDefaultContext    = laDefaultCanvas.getContext("2d");
	laDefaultContext.webkitImageSmoothingEnabled=false;
	if( laAutoResize )
	{
		window.onresize = _laResizeObjectContext;	
		_laResizeObjectContext();
	}
	
	laInitAudio();
	laInitMouse();
	
	laInitReadyCallback();
}

