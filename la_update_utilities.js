//
// Copyright 2013,  Stefan Lundgren,  onehourgroup.com  
//

function laFollowPositionPath(object, target )
{
	var absY = Math.abs(target.position.y-object.position.y);
	var absX = Math.abs(target.position.x-object.position.x);
	object.count.y -= absY;
	object.count.x -= absX;

	if(	object.count.y <=0  && absY != 0 )
	{
		object.count.y += Math.max(absX, absY);
		if(	target.position.y >	object.position.y	)
			object.position.y++;
		else
			object.position.y--;
	}

	if(	object.count.x <=0 && absX != 0 )
	{
		object.count.x += Math.max(absX, absY);
		if(	target.position.x >	object.position.x	)
			object.position.x++;
		else
			object.position.x--;
	}
}


function laIsPositionCollission( object1,	object2, range )
{
	range =	2;
	
	var dx = object1.position.x - object2.position.x;
	var dy = object1.position.y - object2.position.y;

	if(	dx < range && dx > -range	&&
		dy < range && dy > -range	)
		return true;

	return false;
}


function laGetDistance(dx,dy)
{
	return Math.sqrt( dx*dx + dy*dy );
}


function laGetPositionAngleToTarget(object, target)
{
	dx = target.position.x - object.position.x;
	dy = target.position.y - object.position.y;

	angle =	0;

	if(	dx != 0	)
	{
		angle =	Math.asin( dx /	laGetDistance( dx, dy ) );

		if(	angle == Math.NaN )
			angle =	0;
	}

	if(	dy >= 0	)
		angle =	Math.PI	- angle;

	return angle;
}



function laGetClosestObject( object, objectList )
{
    var distance = -1;
    var result = -1;

    for(var	j =	0; j < objectList.length ; j ++)
    {
		currO = objectList[j]
    	if ( 'health' in currO && 'render' in currO)
		{
			var value = laGetDistance(currO.position.x-object.position.x, currO.position.y-object.position.y );
			if( distance == -1 || distance > value)
			{
				distance = value;
				result = currO;
			}
		}
    }

    return result;
}

function laGetWeakestObject( object, objectList )
{
    var health = -1;
    var result = -1;

    for(var	j =	0; j < objectList.length ; j ++)
    {
    	if ( 'health' in objectList[j] && 'render' in objectList[j])
		{
			var value = objectList[j].health;
			if(health == -1 && value > 0)
			{
				health = value;
				result = objectList[j];
			}
			else if( health > value && value > 0)
			{
				health = value;
				result = objectList[j];
			}
		}
    }

    return result;
}


function laGetStrongestObject( object, objectList )
{
    var health = -1;
    var result = -1;

    for(var	j =	0; j < objectList.length ; j ++)
    {
    	if ( 'health' in objectList[j] && 'render' in objectList[j])
		{
			var value = objectList[j].health;
			if(health == -1 )
			{
				health = value;
				result = objectList[j];
			}
			else if( health < value)
			{
				health = value;
				result = objectList[j];
			}
		}
    }

    return result;
}


function laGetFastestObject( object, objectList )
{
    var speed = -1;
    var result = -1;

    for(var	j =	0; j < objectList.length ; j ++)
    {
    	if ( 'speed' in objectList[j] && 'render' in objectList[j])
		{
			var value = objectList[j].speed;
			if(speed == -1 )
			{
				speed = value;
				result = objectList[j];
			}
			else if( speed < value)
			{
				speed = value;
				result = objectList[j];
			}
		}
    }

    return result;
}

function laGetSlowestObject( object, objectList )
{
    var speed = -1;
    var result = -1;

    for(var	j =	0; j < objectList.length ; j ++)
    {
    	if ( 'speed' in objectList[j] && 'render' in objectList[j])
		{
			var value = objectList[j].speed;
			if(speed == -1 )
			{
				speed = value;
				result = objectList[j];
			}
			else if( speed > value)
			{
				speed = value;
				result = objectList[j];
			}
		}
    }

    return result;
}

function laGetObjectsWithinRange( pos, range, layerId )
{
    var res = [];

	var layer = laCurrentObjectContext.renderObjects[ layerId];
	
	for(var	i = 0; i < layer.objectCount; i++)
	{
		var currObj = layer.objects[i];

		obj = currObj.position;
		
		
		if ( obj.x < pos.x - range ) 
			continue;
			
		if ( obj.x > pos.x + range )
			continue;
			
		if ( obj.y < pos.y - range ) 
			continue;
			
		if ( obj.y > pos.y + range )
			continue;
		
		if( laGetDistance( obj.x-pos.x, obj.y-pos.y ) <= range )
			res[res.length] = currObj;
	}


    return res;
}

function laPositionInsideBoundary(object, pos)
{
	var position = object.position;
	var boundary = object.boundary;
	
	if( object.alignment.type == "top-left" )
	{
		if(pos.x >=	(position.x) &&	pos.x <= (position.x + boundary.width))
			if(pos.y >=	(position.y) && pos.y <= (position.y + boundary.height))
				return true;	
	}
	else
	{
		if(pos.x >=	(position.x - boundary.width/2) &&	pos.x <= (position.x + boundary.width/2))
			if(pos.y >=	(position.y - boundary.height/2) && pos.y <= (position.y + boundary.height/2))
				return true;
	}
	
	return false;
}



function laPlacedOnObject( pos)
{
	// Check for other cannon on the same possition
	for(var	layerIndex = 0; layerIndex < laCurrentObjectContext.renderObjects.length; layerIndex++)
	{    
		var layer = laCurrentObjectContext.renderObjects[ layerIndex];

		// Render objects
		for(var	i =	0; i < layer.objectCount; i++)
		{
			var x = 0;
			var y = 0;
			var object = layer.objects[i];

			if (object.position.x ==	pos.x && object.position.y == pos.y)
				return true;
		}
	}
	return false;
}

function laUnselectOther( object ) 
{
	for(var	layerIndex = 0; layerIndex < laCurrentObjectContext.renderObjects.length; layerIndex++)
	{
		var layer = laCurrentObjectContext.renderObjects[ layerIndex];
		for(var	i = 0; i < layer.objectCount; i++)
		{
			var tmpObj = layer.objects[i];

			if( 'selected' in tmpObj )
			{
				laSetSelected(tmpObj, false );
			}
		}
	}
	
	laSetSelected( object, true);
}