//
// Copyright 2013,  Stefan Lundgren,  onehourgroup.com  
//



// Updates rotation with rotation speed
function laUpdateRotation( object )
{

	rotation = object.rotation.angle;
	
	if( 'rotation' in object )
	{
		rotation += object.rotation.speed;	
		object.rotation.angle = rotation;
	}	
}

// Updates both position and rotation
function laUpdatePositionAndRotationToTarget( object )
{
	var rotation = 0;

	if( 'targets' in object && object.targets.length > 0 )
	{
		var targets = object.targets;
		var target	= targets[0];

		rotation    = laGetPositionAngleToTarget(object, target);
		
		if( 'rotation' in object )
			rotation += object.rotation.speed;	
				
		if( 'offset' in object )
			rotation += object.offset.rotation;
					
		rotation -= rotation % (Math.PI * 2 / 256);		
		
		
		if( 'stopTurn' in object && Math.abs( object.rotation.angle - rotation ) > Math.PI / 10 )
		{

		}
		else if( 'speed' in object )
		{	
			object.speedCount += object.speed;	
			for(var	k =	0; k < object.speedCount;	k++)
			{
				laFollowPositionPath( object, target);
				object.speedCount--;	
				
				if( !laIsPositionCollission( object, target, object.speed ) )
					continue;

				targets.shift();
				
				if( targets.length == 0 )
				{
					if( 'targetAction' in object )
						object.targetAction( object, target );
						
					break;
				}
				
				if( 'checkPointAction' in object )
					object.checkPointAction( object, target, targets[0] ); 
					
				target = targets[0];							
			}
		}	
	}	
	else
	{
		if( 'rotation' in object )
		{
			rotation += object.rotation.speed;	
		}		
	}

	
	if( object.rotation.angle != rotation )
	{	
		if( 'stopTurn' in object && Math.abs( object.rotation.angle - rotation ) > Math.PI / 80 )
		{
			if( rotation > object.rotation.angle ) 
				rotation = object.rotation.angle + Math.PI / 160;
			else if( rotation < object.rotation.angle ) 
				rotation = object.rotation.angle - Math.PI / 160;
		}
		
		object.rotation.angle = rotation;
		
		laRefreshPreRender( object );
	}				
}

// Updates only position
function laUpdatePositionToTarget( object )
{
	var rotation = 0;

	if( 'targets' in object && object.targets.length > 0 )
	{
		var targets = object.targets;
		var target	= targets[0];

		if( !('speed' in object) )
			return;
			
		object.speedCount += object.speed;	
		for(var	k =	0; k < object.speedCount;	k++)
		{
			laFollowPositionPath( object, target);
			object.speedCount--;	
			
			if( !laIsPositionCollission( object, target, object.speed ) )
				continue;

			targets.shift();
			
			if( targets.length == 0 )
			{
				if( 'targetAction' in object )
					object.targetAction( object, target );
					
				break;
			}
			
			if( 'checkPointAction' in object )
				object.checkPointAction( object, target, targets[0] ); 
				
			target = targets[0];
						
		}	
	}	
}

// Updates only object aim rotation.
function laUpdateRotationToTarget( object )
{

	var rotation = 0;

	if( 'targets' in object && object.targets.length > 0 )
	{
		var targets = object.targets;
		var target	= targets[0];

		rotation    = laGetPositionAngleToTarget(object, target);
		
		if( 'rotation' in object )
			rotation += object.rotation.speed;	
				
		if( 'offset' in object )
			rotation += object.offset.rotation;
					
		rotation -= rotation % (Math.PI * 2 / 256);		
	}	

	
	if( object.rotation.angle != rotation )
	{	
		if( 'stopTurn' in object && Math.abs( object.rotation.angle - rotation ) > Math.PI / 80 )
		{
			if( rotation > object.rotation.angle ) 
				rotation = object.rotation.angle + Math.PI / 160;
			else if( rotation < object.rotation.angle ) 
				rotation = object.rotation.angle - Math.PI / 160;
		}
		
		object.rotation.angle = rotation;
		
		laRefreshPreRender( object );
	}				
}



function laToggleFlash( object )
{
	if( object.visible )
	{
		laHide(object);
		laSetTimer( object, object.flash.inactiveTime, laToggleFlash );
	}
	else
	{
		laShow(object);
		laSetTimer( object, object.flash.activeTime, laToggleFlash );
	}
}

function laCheckTimer( object )
{
	if( 'timer' in object )
	{
		if( object.timer.frameCount <= 0 )
		{
			var callback = object.timer.callback;

			laRemoveUpdateFunction( object, 'timer' );
			delete object.timer;
			
			callback( object );
		}
		else 
		{
			object.timer.frameCount--;
		}
	}
}


