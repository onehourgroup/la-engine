//
// Copyright 2013,  Stefan Lundgren,  onehourgroup.com  
//


var laMuted   = false;
var laPaused  = false;
var sounds    = {};
sounds.waves  = [];
	
var laSoundCache = 4;
	
function laInitAudio()
{
	sounds       = {};
	sounds.waves = [];
}

function laSetMute( mute )
{
	laMuted = mute;
	for( var i= 0; i< sounds.waves.length; i++ )
	{
		for( var c = 0; c < sounds.waves[i].cache.length; c++ )
		{
			if( window.cordova )
			{
				if( laMuted )
					sounds.waves[i].cache[c].setVolume( 0 );
				else
					sounds.waves[i].cache[c].setVolume( 1 );
					
					
			}
			else
				sounds.waves[i].cache[c].muted = laMuted;
		}
	}
}

function laSetPause( pause )
{
	for( var i= 0; i< sounds.waves.length; i++ )
	{
		for( var c = 0; c < sounds.waves[i].cache.length; c++ )
		{
			if( !sounds.waves[i].cache[c].ended && pause )
				sounds.waves[i].cache[c].pause();
			else if( sounds.waves[i].cache[c].ended && !pause )
				sounds.waves[i].cache[c].pause();
		}
	}
}

function laGetMute( )
{
	return laMuted;
}


function laAddSound( source )
{
	var cache = { cache:[], next:0 };
	
	sounds.waves[sounds.waves.length] = cache;
	
	for( var i = 0; i < laSoundCache; i++ )
	{
		// Check for phonegap/cordova
		if( window.cordova )
		{
			fixed_source = "/android_asset/www/"+source;
			laLog ("Adding sound:"+fixed_source);
			cache.cache[i] = new Media( fixed_source );
		}
		else
		{
			var fixed_source = "./"+source;
			laLog ("Adding sound:"+fixed_source);
			cache.cache[i] = new Audio( fixed_source );
		}
	}
	
	return sounds.waves.length-1;
}

function laLoopSound( id )
{
	if( id >= sounds.waves.length )
		return;
	
	var theSound = sounds.waves[id];
	
	// Check for phonegap/cordova
	if( window.cordova )
	{
		theSound.cache[theSound.next].play({numberOfLoops:"infinite"});
	}
	else
	{
		if( theSound.cache.length > 0 )
		{
			if (typeof theSound.cache[theSound.next].loop == 'boolean')
			{
				theSound.cache[theSound.next].loop = true;
			}
			else
			{
				theSound.cache[theSound.next].addEventListener('ended', function() {
					this.currentTime = 0;
					this.play();
				}, false);
			}
		
			theSound.cache[theSound.next].play();
		}
	}
}



function laPlaySound( id )
{
	try
	{
		if( id >= sounds.waves.length )
			return;
		
		var theSound = sounds.waves[id];
		
		if( theSound.cache.length > 0 )
		{
			if( !theSound.cache[theSound.next].ended )
			{
				theSound.cache[theSound.next].pause();
				theSound.cache[theSound.next].currentTime = 0;
			}
			
			theSound.cache[theSound.next].muted = laMuted;
			theSound.cache[theSound.next].play();
			theSound.next++;
			
			if( theSound.cache.length == theSound.next )
				theSound.next = 0;
			
		}
	}
	catch(err)
	{
		dbgLastException = err.message;
	}
}


function laStopSound( id )
{
	if( id >= sounds.waves.length || sounds.waves.length == 0 )
		return;

	var theSound = sounds.waves[id];
	
	if( theSound == null )
		return;
	
	for( var i=0; i < theSound.cache.length; i++ )
	{
		theSound.cache[i].pause();
	}
}

