//
// Copyright 2013,  Stefan Lundgren,  onehourgroup.com  
//

function laGetFontStringWidth( font, text )
{
	return text.length * font.letterWidth;
}

function laInitFont( fontImage, letterString, letterWidth, letterHeight )
{
	var font = {};
	
	if( letterString == null )
		letterString = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ.,!?�$�'\"+-()/:;<>=@#&\\*";
		
	font.letterString = letterString;
	
	if( letterWidth == null )
		letterWidth = 20;
		
	font.letterWidth = letterWidth;
		
	if( letterHeight== null )
		letterHeight = 30;		
		
	font.letterHeight = letterHeight;
	font.fontArray = [];

    for (var counter =0; counter < letterString.length ;counter++ ) 
    { 
		//laLog ("Parsing letter: " + letterString.charAt( counter ));
    	var canvas    = document.createElement('canvas');
    	canvas.width  = letterWidth;
    	canvas.height = letterHeight;
    	
    	var context = canvas.getContext('2d');
    	
    	context.save()
    	var yCoord = Math.floor((counter / ( fontImage.width / letterWidth )));
    	var xCoord = counter - yCoord * fontImage.width / letterWidth;
    	
    	var xOff = -xCoord*letterWidth;
    	var yOff = -yCoord*letterHeight;
    	//laLog ("Parsing letter: " + xOff + " " + yOff);
    	context.translate( xOff , yOff );
    	context.drawImage(fontImage, 0, 0);
        context.restore()
        font.fontArray[counter] = canvas;        
    } 		
    
    return font;
}

function laDrawFontTextXY( context, font, text, x, y, fit )
{
	var xPos = 0;
	
	if ( fit == null )
	{
		fit = 'top-left';
	}

	var xOff = 0;
	
	if( fit == 'center' )
		xOff = -Math.round( laGetFontStringWidth( font, text )/2 );

	text = text.toUpperCase();
    for (var counter =0 ;counter < text.length ;counter++ ) 
    {
		var theChar = text.charAt( counter );
		var theIndex = font.letterString.indexOf( theChar );
		//laLog ("Letter: " + theChar + " " + theIndex);
		
		if( theIndex < 0 )
		{
			xPos += font.letterWidth;
			continue;
		}
			
		var canvas = font.fontArray[ theIndex];

		//laLog ("Canvas: " + canvas.width );
		context.drawImage( canvas, xPos + x + xOff , y );
		xPos += canvas.width;
		
		//laLog ("Xpos: " + xPos );
    }
}

