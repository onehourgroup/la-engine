//
// Copyright 2013,  Stefan Lundgren,  onehourgroup.com  
//

function laAddEvent( key )
{
	laCurrentObjectContext.events[key] = [];
}


function laSubscribeEvent( key, object, callback)
{
	if( key in laCurrentObjectContext.events )
	{
		laCurrentObjectContext.events[key][laCurrentObjectContext.events[key].length] = {object:object, callback:callback};
	}	
}

function laSignalEvent( key )
{
	if( key in laCurrentObjectContext.events )
	{
		var subscribers = laCurrentObjectContext.events[key]
		for(var	i = 0; i < subscribers.length; i++)
		{
			subscribers[i].callback( subscribers[i].object, key );
		}
	}
}