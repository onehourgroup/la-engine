//
// Copyright 2013,  Stefan Lundgren,  onehourgroup.com  
//


// This is the engine object update core.

function laUpdateAllObjects()
{
	var laEngineTargetUpdateCount = Math.round( (new Date().getTime() - laEngineStartTime) / laUpdateDelay );
	
	var updateSpeed = Math.round( laEngineUpdateSpeed );
					
	// Update all objects
	for( var u= laEngineUpdateCount; u < laEngineTargetUpdateCount; u++ )
		for( var us= 0; us < Math.round( updateSpeed ); us++ )	
			laUpdateObjectList();
			
	laEngineUpdateCount = laEngineTargetUpdateCount;			
}

// NEW CODE WHERE UPDATE AND RENDER ARE SEPARATED
function laUpdateObjectList( objectList )
{
	var updateObjects = laCurrentObjectContext.updateObjects;
	
	for(var i =	0; i < updateObjects.length; i++)
	{		
		var object = updateObjects[i];

		if( object.remove || !object.active || ('update' in object && object.update.length == 0 ))
		{
			updateObjects.splice( i, 1 );
			i--;
			continue;
		}
		
		object.laUpdateObject();
	}	
}



// Add render function to object. 
function laAddUpdateFunction( object, key, func )
{
	if( !('update' in object) )
	{
		object.update = {};
		_laAddToUpdateList( object );
	}
		
	object.update[key] = func;
}

// REmove render function to object. 
function laRemoveUpdateFunction( object, key )
{
	if( 'update' in object )
		delete 	object.update[key];
}


// Calls all updater callbacks for an object.
function laUpdateObject( object )
{
	if( object.delayFrames-- > 0 )
		return;

	for( u in object.update )
	{
		object.update[u]( object );
	}
}

// Removes an object at position index from a layer 
function laRemoveListObject( objectList, index )
{
	if( objectList.objectCount - 1 != index )
	{
		objectList.objects[index] = objectList.objects[objectList.objectCount-1];
		objectList.objects[objectList.objectCount-1] = {};
	}
		
	objectList.objectCount--;
}

//
// Internal functions used by engine
//

function _laAddToUpdateList( object )
{
	if( object.active && 'update' in object)
		laCurrentObjectContext.updateObjects[laCurrentObjectContext.updateObjects.length] = object;
}
