//
// Copyright 2013,  Stefan Lundgren,  onehourgroup.com  
//

var	dbgLastUpdate					= new Date().getTime();
var dbgEngineFrameProcessTime		= 0;
var dbgEngineMaxFrameProcessTime	= 0;

var dbgCanvas						= document.createElement('canvas');
var dbgCtx                          = dbgCanvas.getContext('2d');

var dbgBrowserFrameProcessTime		= 0;
var dbgBrowserMaxFrameProcessTime	= 0;
var dbgBrowserMaxFrameFrameId    	= 0;
var dbgLastException            	= "";

var laDebugModeOff  = 0;
var laDebugModePerf = 1;
var laDebugModeInfo = 2;
var laDebugModeEx   = 3;

var dbgCustom = ""



function laDebugReset()
{
	dbgBrowserMaxFrameProcessTime = 0;
	dbgEngineMaxFrameProcessTime  = 0;  
	dbgBrowserMaxFrameFrameId	  = 0;
	dbgLastUpdate                 = new Date().getTime(); 
}           

function _laResizeDebug()
{
	if( laDbgMode != laDebugModePerf )
	{
		dbgCanvas.width		= laCanvasWidth;
		dbgCanvas.height	= laCanvasHeight;
	}
	else
	{
		dbgCanvas.width		= Math.round( 70 * laGlobalScale );
		dbgCanvas.height	= Math.round( 20 * laGlobalScale );
	}
}
            
function laDebugFrameStart()
{
	var updateTime = new Date().getTime();
	dbgBrowserFrameProcessTime = updateTime - dbgLastUpdate;
	if( dbgBrowserFrameProcessTime > dbgBrowserMaxFrameProcessTime )
	{
		dbgBrowserMaxFrameProcessTime = dbgBrowserFrameProcessTime;
		dbgBrowserMaxFrameFrameId     = laEngineFrameCount;
	}
	dbgLastUpdate = updateTime;
	

}

function laDebugFrameEnd()
{
	dbgEngineFrameProcessTime = _laGetFrameProcessTime( dbgLastUpdate );
	if( dbgEngineFrameProcessTime > dbgEngineMaxFrameProcessTime )
		dbgEngineMaxFrameProcessTime = dbgEngineFrameProcessTime;		
}

function laDebugDisplayInfo()
{
	if( dbgCanvas.width == 0 || dbgCanvas.height == 0 )
		return;
			
	if( laEngineFrameCount % 10 == 0 )
	{

		dbgCtx.save();
		dbgCtx.clearRect (0,0,dbgCanvas.width,dbgCanvas.height);
		dbgCtx.scale(laGlobalScale, laGlobalScale );

		

		dbgCtx.fillStyle = "#0f0";
		dbgCtx.font = 'bold 15px sans-serif';
		var row = 1;
		var rowHeight = 16;
		
		var x = 0;
		
		if( laDbgMode != laDebugModePerf )
		{
			dbgCtx.fillText("Resolutions: " ,10,x + row++*rowHeight );
			dbgCtx.fillText("[screen]: " + window.screen.width + ":" + window.screen.height ,10,x + row++*rowHeight );
			dbgCtx.fillText("[browser]: " + window.innerWidth + ":" + window.innerHeight ,10,x + row++*rowHeight );
			dbgCtx.fillText("[canvas]: " + laCanvasWidth + ":" + laCanvasHeight ,10,x + row++*rowHeight );
			dbgCtx.fillText("[engine]: " + laEngineResolutionWidth + ":" + laEngineResolutionHeight ,10,x + row++*rowHeight );
			dbgCtx.fillText("[margins]: " + laMarginLeft + ":" + laMarginTop ,10,x + row++*rowHeight );
			dbgCtx.fillText("[scale engine / browser]: " + roundNumber(laGlobalScale,2) + " / " + roundNumber(laBrowserScale,2) ,10,x + row++*rowHeight );
			
			row++;
			dbgCtx.fillText("Current position: " ,10,x + row++*rowHeight );
			dbgCtx.fillText("[touch]: " + laTouch.x + ":" + laTouch.y + " Down(" + laTouch.xDown + ":" + laTouch.yDown + ") Up(" + laTouch.xUp + ":" + laTouch.yUp + ")",10,x + row++*rowHeight );
			dbgCtx.fillText("[mouse]: " + laMouse.x + ":" + laMouse.y + " Down(" + laMouse.xDown + ":" + laMouse.yDown + ") Up(" + laMouse.xUp + ":" + laMouse.yUp + ")",10,x + row++*rowHeight );
			dbgCtx.fillText("[engine]: " + laEngineMouse.x + ":" + laEngineMouse.y ,10,x + row++*rowHeight );
			coord = laGetGridXY(laEngineMouse.x,laEngineMouse.y );
			dbgCtx.fillText("[grid]: " + coord.x + ":" + coord.y ,10,x + row++*rowHeight );

		
			row++;
			dbgCtx.fillText("Frame time: " ,10,x + row++*rowHeight );
			dbgCtx.fillText("[target frames/s]: " + Math.round(1000/laEngineFrameDelay),10,x + row++*rowHeight );
		}
		
		dbgCtx.fillText("[fps]: " + Math.round(1000/dbgBrowserFrameProcessTime+0.75),10,x + row++*rowHeight );
		
		if( laDbgMode != laDebugModePerf )
		{
			dbgCtx.fillText("[engine ms/frame]: " + dbgEngineFrameProcessTime,10,x + row++*rowHeight );
			dbgCtx.fillText("[engine max ms/frame]: " + dbgEngineMaxFrameProcessTime,10,x + row++*rowHeight );
			
			dbgCtx.fillText("[browser ms/frame]: " +  dbgBrowserFrameProcessTime,10,x + row++*rowHeight );
			dbgCtx.fillText("[browser max ms/frame],(frameId): " +  dbgBrowserMaxFrameProcessTime+ "(" + dbgBrowserMaxFrameFrameId + ")",10,x + row++*rowHeight );

	
			var totalObj = 0;
			var activeObj = 0;
			
			for(var	i =	0; i < laCurrentObjectContext.renderObjects.length;  i++)
			{
				totalObj += laCurrentObjectContext.renderObjects[i].objects.length;
				activeObj += laCurrentObjectContext.renderObjects[i].objectCount;
			}
			
			row++;
			dbgCtx.fillText("Objects: " ,10,x + row++*rowHeight );
			dbgCtx.fillText("[object context]: " + laCurrentObjectContextId,10,x + row++*rowHeight );		
			dbgCtx.fillText("[total layers]: " + laCurrentObjectContext.renderObjects.length,10,x + row++*rowHeight );		
			dbgCtx.fillText("[total render objects]: " + totalObj,10,x + row++*rowHeight );
			dbgCtx.fillText("[active render objects]: " + activeObj,10,x + row++*rowHeight );    
			dbgCtx.fillText("[removed render objects]: " + (totalObj-activeObj),10,x + row++*rowHeight ); 
			dbgCtx.fillText("[active update objects]: " + laCurrentObjectContext.updateObjects.length,10,x + row++*rowHeight );       
			dbgCtx.fillText("[exception]: " + dbgLastException,10,x + row++*rowHeight );
		}
		dbgCtx.restore();		
		
	}
	
	laDefaultContext.drawImage(dbgCanvas, 10, 10);	

}

function laToggleDebug()
{
	if( laDbgMode == laDebugModeOff )
		laDbgMode = laDebugModeInfo;
	else if( laDbgMode == laDebugModeInfo )
	{
		laDbgMode = laDebugModeEx;
	}
	else if( laDbgMode == laDebugModeEx )
	{
		laDbgMode = laDebugModePerf;
	}
	else
	{
		laDbgMode = laDebugModeOff;
		laDebugReset();
	}
	_laResizeDebug();
}

// If we don't have a firebug console, create empty log function
// browsers get confused otherwise...
if( typeof console === "undefined" )
{
	console = { log: function () { } };
}

function laLog( msg )
{
	laConsoleLogFB( msg );
}

// log to firebug console
function laConsoleLogFB( msg )
{
	console.info( msg );
}