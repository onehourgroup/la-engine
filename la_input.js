//
// Copyright 2013,  Stefan Lundgren,  onehourgroup.com  
//


var laMouse							= {x:-1, y:-1, xDown:-1, yDown:-1, xUp:-1, yUp:-1, mouseTouch:true };
var laTouch							= {x:-1, y:-1, xDown:-1, yDown:-1, xUp:-1, yUp:-1, mouseTouch:true };
var laEngineMouse                   = {x:-1, y:-1, xDown:-1, yDown:-1, xUp:-1, yUp:-1, mouseTouch:true };


var laMouseOverCheckPerSecond = 3;

var laMouseEventDownCb        = null;
var laMouseEventUpCb          = null;


function _laGetMouseDownSubscribers()
{
	var mouseTouch = laCurrentObjectContext.mouseTouch;
	if( !('laMouseDownSubscribers' in mouseTouch) )
		mouseTouch.laMouseDownSubscribers = [];
		
	return mouseTouch.laMouseDownSubscribers;
}

function _laGetMouseUpSubscribers()
{
	var mouseTouch = laCurrentObjectContext.mouseTouch;
	if( !('laMouseUpSubscribers' in mouseTouch) )
		mouseTouch.laMouseUpSubscribers = [];

	return mouseTouch.laMouseUpSubscribers;		
}

function _laGetMouseOverSubscribers()
{
	if( !('laMouseOverSubscribers' in laCurrentObjectContext.mouseTouch) )
		laCurrentObjectContext.mouseTouch.laMouseOverSubscribers = [];

	return laCurrentObjectContext.mouseTouch.laMouseOverSubscribers;		
}



function laInitMouse()
{
	laHandleInputEvents( laDefaultCanvas );
	
    document.onkeyup   = laKeyPress;	
}

function laHandleInputEvents( canvas )
{
    canvas.onmousemove = laMouseMove;
    canvas.onmouseup   = laMouseUp;
	canvas.onmousedown = laMouseDown;
	
	canvas.ontouchstart= laTouchStart;
	canvas.ontouchend  = laTouchEnd;
	canvas.ontouchmove = laTouchMove;
}

function laSetMouseEventDownCb( callback )
{
	laMouseEventDownCb        = callback;
}


function laSetMouseEventUpCb( callback )
{
	laMouseEventUpCb        = callback;
}




function _laUpdateEngineMouse( input )
{
	if( laResizeByStyle )
	{
		laEngineMouse.x = Math.round( input.x / laBrowserScale );
		laEngineMouse.y = Math.round( input.y / laBrowserScale );	
	}
	else
	{
		laEngineMouse.x = Math.round( input.x / laGlobalScale );
		laEngineMouse.y = Math.round( input.y / laGlobalScale );
	}
	
	

}
function _laMouseOverCheck()
{
	var mouseCheck = laEngineUpdateCount * laMouseOverCheckPerSecond / laUpdatesPerSecond;
	
	if(  mouseCheck - Math.round( mouseCheck ) )
	{
		_laPerformMouseOverCheck();	
	}
}


function _laRefCopy( list )
{
	var newList = (list instanceof Array) ? [] : {};
  	
  	for(var	i = 0; i < list.length; i++)
	{
		newList[i] = list[i];
	}
  
	return newList;
}


function laMouseTouchUp(input) 
{
	var subscribers = _laRefCopy( _laGetMouseUpSubscribers() );
	
	var mouseEventCalled = false;
	
	// process copy of subscribers since events may add to list.
	for(var	i = 0; i < subscribers.length; i++)
	{
		var tmpObj = subscribers[i];

		if( 'active' in tmpObj && tmpObj.active == false )
			continue;
						
		var pos = {x:input.x, y:input.y };		
		
		if( 'mouseUpAction' in tmpObj )
		{
			if( 'boundary' in tmpObj && laPositionInsideBoundary( tmpObj, pos ) )
			{
				tmpObj.mouseUpAction( tmpObj, pos );
				mouseEventCalled = true;
			}
		}
		else if( 'anyMouseUpAction' in tmpObj )
		{
			tmpObj.anyMouseUpAction( tmpObj, pos );
			mouseEventCalled = true;
		}

	}


	for(var	j = 0; j < _laGetMouseUpSubscribers().length; j++)
	{
		var tmpObj = _laGetMouseUpSubscribers()[j];
		
		if( !('mouseUpAction' in tmpObj) && !( 'anyMouseUpAction' in tmpObj ))
		{
			_laGetMouseUpSubscribers().splice( j, 1 );
			j--;
		}
	}

	

	// Must be in its own loop cause mouse up events may unselect objects before mouse up was trigered.
	// TODO this code should not be here in la_input.
	if( ! mouseEventCalled )
	{
		var length = laCurrentObjectContext.renderObjects.length;
		for(var	layerIndex = 0; layerIndex < length; layerIndex++)
		{
			var layer = laCurrentObjectContext.renderObjects[ layerIndex];
			for(var	i = 0; i < layer.objectCount; i++)
			{
				var tmpObj = layer.objects[i];

				if( 'active' in tmpObj && tmpObj.active == false )
					continue;
						
				var pos = {x:input.x, y:input.y };
				
				if( 'selected' in tmpObj && tmpObj.selected == true )
				{
					laSetSelected( tmpObj, laPositionInsideBoundary( tmpObj, pos ) );
				}
				else if( 'selected' in tmpObj )
				{
					laSetSelected( tmpObj, laPositionInsideBoundary( tmpObj, pos ) );
				}
			}
		}	
	}

	if( mouseEventCalled && laMouseEventUpCb != null )
		laMouseEventUpCb();

}



function laMouseTouchDown(input) 
{
	var subscribers = _laRefCopy(_laGetMouseDownSubscribers());

	var mouseEventCalled = false;
			
	// process copy of subscribers since events may add to list.
	for(var	i = 0; i < subscribers.length; i++)
	{
		var tmpObj = subscribers[i];

		if( 'active' in tmpObj && tmpObj.active == false )
			continue;
						
		var pos = {x:input.x, y:input.y };		
		
		if( 'mouseDownAction' in tmpObj )
		{
			if( 'boundary' in tmpObj && laPositionInsideBoundary( tmpObj, pos ) )
			{
				tmpObj.mouseDownAction( tmpObj, pos );
				mouseEventCalled = true;

			}
		}
		else if( 'anyMouseDownAction' in tmpObj )
		{
			tmpObj.anyMouseDownAction( tmpObj, pos );
			mouseEventCalled = true
			
		}
	}

	for(var	j = 0; j < _laGetMouseDownSubscribers().length; j++)
	{
		var tmpObj = _laGetMouseDownSubscribers()[j];
		
		if( !('mouseDownAction' in tmpObj) && !( 'anyMouseDownAction' in tmpObj ))
		{
			_laGetMouseDownSubscribers().splice( j, 1 );
			j--;
		}
	}
	
	if( mouseEventCalled && laMouseEventDownCb )
	{
		laMouseEventDownCb();
	}
}


function _laPerformMouseOverCheck() 
{
	var subscribers = _laGetMouseOverSubscribers();
	
	// process copy of subscribers since events may add to list.
	for(var	i = 0; i < subscribers.length; i++)
	{
		var tmpObj = subscribers[i];

		if( 'active' in tmpObj && tmpObj.active == false )
			continue;
						
		if( 'mouseOverAction' in tmpObj )
		{
			var inside = false;
			
			if( 'boundary' in tmpObj && laPositionInsideBoundary( tmpObj, laEngineMouse ) )
			{
				inside = true;
			}
			
		
			if( tmpObj.mouseOverAction.started == true )
			{
				if( inside == false )
				{
					tmpObj.mouseOverAction.actionEnd( tmpObj );
					tmpObj.mouseOverAction.started = false;
				}
				
			}
			else
			{
				if( inside )
				{
					tmpObj.mouseOverAction.started = true;
					tmpObj.mouseOverAction.actionStart( tmpObj );
				}
				
			}
		}


	}

	for(var	j = 0; j < _laGetMouseOverSubscribers().length; j++)
	{
		if( !('mouseOverAction' in tmpObj) )
		{
			_laGetMouseOverSubscribers().splice( j, 1 );
			j--;
		}
	}

}


function laKeyPress(e) {
    var kC  = (window.event) ?    // MSIE or Firefox?
    event.keyCode : e.keyCode;
    var Esc = (window.event) ?
        27 : e.DOM_VK_ESCAPE; // MSIE : Firefox
    if(kC==Esc)
    {
		laToggleDebug();
        
    }
    else if( e.charCode==68 )
    {
        laToggleDebug();    
    }
}

function laMouseUp( event )
{
	laMouse.xUp = laMouse.x;
	laMouse.yUp = laMouse.y;
	
    if( !laIsTouch() )
    {
		_laMouseOverCheck();
	}
	else
	{
		laLog("Do not notify mouse events when using touch events.");
		return;
	}	
		
	_laUpdateEngineMouse( laMouse );		
	laMouseTouchUp( laEngineMouse );
}


function laMouseDown( event )
{
	laMouse.xDown = laMouse.x;
	laMouse.yDown = laMouse.y;
	laMouse.xUp   = -1;
	laMouse.yUp   = -1;
		
    if( !laIsTouch() )
    {
		_laMouseOverCheck();
	}
	else
	{
		laLog("Do not notify mouse events when using touch events.");
		return;
	}
		
	
	_laUpdateEngineMouse( laMouse );		
	laMouseTouchDown( laEngineMouse );
}


function laTouchStart(e)
{
	e.preventDefault();
	event = e.changedTouches[0];
	laTouch.x = event.pageX - laMarginLeft;
	laTouch.y = event.pageY - laMarginTop;
	
	laTouch.xDown = laTouch.x;
	laTouch.yDown = laTouch.y;
	laTouch.xUp   = -1;
	laTouch.yUp   = -1;

	_laUpdateEngineMouse( laTouch );	
		
	laMouseTouchDown( laEngineMouse )	
}

function laTouchEnd(e)
{
	
	e.preventDefault();
	event = e.changedTouches[0];
	laTouch.x = event.pageX - laMarginLeft;
	laTouch.y = event.pageY - laMarginTop;
	
	laTouch.xUp = laTouch.x;
	laTouch.yUp = laTouch.y;

	_laUpdateEngineMouse( laTouch );

	
	laMouseTouchUp( laEngineMouse )
	
	

}

function laTouchMove(e) 
{
	e.preventDefault();
	event = e.changedTouches[0];
	laTouch.x = event.pageX - laMarginLeft;
	laTouch.y = event.pageY - laMarginTop;
	
	_laUpdateEngineMouse( laTouch );
}

// Main function to retrieve mouse x-y pos.s
function laMouseMove(event) {
    laMouse.x = event.pageX;
    laMouse.y = event.pageY;
    for (var el = event.target; el;	el = el.offsetParent) {
        laMouse.x -= el.offsetLeft;
    }
    for (var el = event.target; el;	el = el.offsetParent) {
        laMouse.y -= el.offsetTop;
    }
    
    if( !laIsTouch() )
    {
		_laUpdateEngineMouse( laMouse );
	}
}

function laSetAnyMouseClickAction( object, action )
{
	laSetAnyMouseUpAction( object, action );
}



function laSetMouseClickAction( object, action )
{
	laRemoveMouseClickAction( object );
	object.mouseClickAction = action;
	
	laSetMouseDownAction( object, _laClickedOnMe );
}

function _laClickedOnMe( object )
{
	laSetAnyMouseUpAction( object, _laClickRelease );
}


function _laClickRelease( object )
{
	if( laPositionInsideBoundary( object, laEngineMouse ) )
	{
		object.mouseClickAction( object );
	}
	
		
	laRemoveAnyMouseUpAction( object );
}


function laSetAnyMouseDownAction( object, action )
{
	object.anyMouseDownAction = action;
	_laGetMouseDownSubscribers()[_laGetMouseDownSubscribers().length] = object;
}

function laSetMouseDownAction( object, action )
{
	object.mouseDownAction = action;
	_laGetMouseDownSubscribers()[_laGetMouseDownSubscribers().length] = object;
}

function laSetAnyMouseUpAction( object, action )
{
	object.anyMouseUpAction = action;
	_laGetMouseUpSubscribers()[_laGetMouseUpSubscribers().length] = object;
}

function laSetMouseUpAction( object, action )
{
	object.mouseUpAction = action;
	_laGetMouseUpSubscribers()[_laGetMouseUpSubscribers().length] = object;
}

function laSetMouseOverAction( object, actionStart, actionEnd )
{
	object.mouseOverAction = { actionStart:actionStart, actionEnd:actionEnd, started:false };
	_laGetMouseOverSubscribers()[_laGetMouseOverSubscribers().length] = object;
}



function laRemoveMouseClickAction( object )
{
	laRemoveMouseDownAction( object );
	laRemoveAnyMouseUpAction( object );

	delete object.mouseClickAction;
}

function laRemoveMouseUpAction( object )
{
	delete object.mouseUpAction;
}

function laRemoveAnyMouseUpAction( object )
{
	delete object.anyMouseUpAction;
}

function laRemoveMouseDownAction( object )
{
	delete object.mouseDownAction;
}

function laRemoveAnyMouseDownAction( object )
{
	delete object.anyMouseDownAction;
}

function laRemoveMouseOverAction( object )
{
	delete object.mouseOverAction;
}

function laGetMouseTouch()
{
	if ( laTouch.x != -1 )
		return laTouch;
	else
		return laMouse;
}

function laGetEngineMouseTouch()
{
	return laEngineMouse;
}

function laIsTouch()
{
	if ( laTouch.x != -1 )
		return true;
		
	return false;
}